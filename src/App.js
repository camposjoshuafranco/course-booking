import './App.css';
import { AppNavBar } from './components/AppNavBar';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { useEffect, useState } from 'react';

import {Home} from './pages/Home';
import {Register} from './pages/Register';
import { CourseView } from './pages/CourseView';
import { LoginPage } from './pages/Login';
import {  Catalog } from './pages/Courses';
import { ErrorPage } from './pages/Error';
import { LogoutPage } from './pages/Logout';
import { AddCourse } from './pages/AddCourse';
import { UpdateCourse } from './pages/UpdateCourse';
import { UserProvider } from './UserContext';




function App() {

  const [user,setUser] = useState({
    id:null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear()
    setUser({
      id: null,
      isAdmin:null
    })
  }

  useEffect(() => {

    let token = localStorage.getItem('accessToken')

    fetch('https://morning-savannah-68233.herokuapp.com/users/details',{
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(convertedData => {
      
      if (typeof convertedData._id !== "undefined") {
        setUser ({
          id: convertedData._id,
          isAdmin: convertedData.isAdmin
        })
      } else {
        setUser ({
          id: null,
          isAdmin: null
        })
      }
    })
  },[user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/register' element={<Register />} />
          <Route path='/courses' element={<Catalog />} />
          <Route path='/courses/add' element={<AddCourse />} />
          <Route path='/courses/view/:id' element={<CourseView />} />
          <Route path='/courses/update' element={<UpdateCourse />} />
          <Route path='/login' element={<LoginPage />} />
          <Route path='/logout' element={<LogoutPage />} />
          <Route path='*' element={<ErrorPage />} />
        </Routes>
      </Router> 
    </UserProvider>
  );
}

export default App;
