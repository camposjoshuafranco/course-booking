import { Banner } from './../components/Banner'
import { Highlights } from './../components/Highlights'
import { Row, Col, Card, Button } from 'react-bootstrap';

const details = {
    title: '404 Error',
    content: 'Oh no!!!!!'
}

export const ErrorPage = () => {
    return (
        <div>
            <Banner bannerData={details} />
        </div>
        
    )
}
