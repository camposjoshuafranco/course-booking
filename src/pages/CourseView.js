import { Banner } from './../components/Banner';
import { Link, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

const details = {
    title: 'Welcome to the Course Details',
    content: 'Here you can view details of Course'
}

export const CourseView = () => {
    const [courseInfo, setCourseInfo] = useState({
        name: null ,
        description: null,
        price: null
    })

    

    const {id} = useParams();
    console.log(id);

    useEffect(() => {
        fetch(`https://morning-savannah-68233.herokuapp.com/courses/${id}`)
        .then(res => res.json()).then(convertedData => {
            console.log(convertedData);

            setCourseInfo({
                name: convertedData.name,
                description: convertedData.description,
                price: convertedData.price
                
            })

            console.log(courseInfo);
        })
    }, [])


    const enroll = () => {
        return (
            Swal.fire({
    		   icon: "success",
    		   title: 'Enrolled Successfully!',
    		   text: 'Thank you for enrolling to this course'
    		})

        )
    }
    return (
        <div>
            <Banner bannerData={details} />
            <Row className="mb-5 mx-5">
                <Col>
                    <Card >
                        <Card.Body>
                            <Card.Title>
                                <h4>{courseInfo.name}</h4>
                            </Card.Title>
                            <Card.Subtitle>
                                <h6>Description:</h6>
                            </Card.Subtitle>
                            <Card.Text>
                                {courseInfo.description} 
                            </Card.Text>
                            <Card.Text>
                                Price: {courseInfo.price} PHP
                            </Card.Text>
                        </Card.Body>
                        <div className="d-flex justify-content-center m-1">
                        <Button className="btn btn-primary btn-block w-25" onClick={enroll}>
                            Enroll
                        </Button>
                        </div>
                        <div className="d-flex justify-content-center m-1 pb-4">
                            <Link className="btn btn-secondary btn-block w-25" to="/login">Login to Enroll</Link>
                        </div> 
                    </Card>
                </Col>
            </Row>
        </div>
    )
}
