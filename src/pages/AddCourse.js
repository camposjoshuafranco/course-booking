import { Container, Form, Button, InputGroup } from "react-bootstrap";
import Swal from "sweetalert2";

const details = {
    title: 'Register Here',
    content: 'Join us!'
}

const addedCourse = (eventSubmit) => {
    eventSubmit.preventDefault()
    return (
        Swal.fire({
            icon: 'success',
            title: 'Added successful',
        })
        

        
    )
}

export const AddCourse = () => {
    return (
        <>
        <Container className="my-5 py-5 w-75 border rounded">
            <h1 className="text-center">Add Course</h1>
            <Form className="mb-5" onSubmit={ e => addedCourse(e)}>
                <Form.Group>
                    <Form.Label>Course Title</Form.Label>
                    <Form.Control 
                        type="text"
                        placeholder="Enter Title"
                        required
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                        as="textarea" 
                        type="text"
                        placeholder="Enter Description"
                        required
                    />
                </Form.Group>

                <Form.Group>
                    <Form.Label>Price</Form.Label>
                    <InputGroup>
                        <InputGroup.Text>PHP</InputGroup.Text>
                        <Form.Control 
                            type="number"
                            placeholder="Enter Price"
                            required
                        />
                    </InputGroup>
                </Form.Group>

                <Button 
                    className="btn-success btn-block gradientbg mt-5"
                    type="submit"
                >
                    Add Course
                </Button>
            </Form>
            
        </Container>
        </>
        
        
    );
}