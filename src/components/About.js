import { Row, Col, Card } from 'react-bootstrap';


export const About = () => {
    return (
        <Row className="my-3 p-5">
            <Col className="border rounded p-4">
                <h1 className="pb-4">About Me</h1>

                <h3 className="pb-4">Joshua Campos</h3>
                <h4>Full Stack Web Developer</h4>
                <h6 className="pb-4">I am a Full Stack Web Developer from Zuitt Coding Bootcamp</h6>
                    <Card className=" w-50">
                        <Card.Body>
                            <Card.Title>
                                Contact Me   
                            </Card.Title>
                            <Card.Text>
                                <ul>
                                    <li>Email: <a href='/'>camposjoshuafranco@gmail.com</a></li>
                                    <li>Mobile No: 0915-285-9262</li>
                                    <li>Address: Dasmarinas, Cavite, Philippines, 4114</li>
                                </ul>
                                <div className="text-center">
                                    <a className="btn btn-primary" href="https://camposjoshua.github.io/campos-portfolio/">More Info</a>
                                </div>
                            </Card.Text> 
                        </Card.Body>
                    </Card>    
            </Col>  
        </Row>
    )
}