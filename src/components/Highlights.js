import { Row, Col, Card } from 'react-bootstrap';


export const Highlights = () => {
    return (
        <Row className="my-3 px-5">
            <Col xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                        <Card.Title>
                            Learn From Home   
                        </Card.Title>
                        <Card.Text>
                            Insert them details here
                        </Card.Text> 
                    </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card >
                    <Card.Body>
                        <Card.Title>
                            Learn From Home   
                        </Card.Title>
                        <Card.Text>
                            Insert them details here
                        </Card.Text> 
                    </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card>
                    <Card.Body>
                        <Card.Title>
                            Learn From Home   
                        </Card.Title>
                        <Card.Text>
                            Insert them details here
                        </Card.Text> 
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}