import { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export const AppNavBar = () => {

    const { user } = useContext(UserContext)

    return (
        <Navbar variant="dark"  className="gradientbg  text-light" expand="lg">
            <Navbar.Brand className="text-light" > B156 Booking App </Navbar.Brand>
            <Navbar.Toggle aria-controls='basic-navbar-nav' />
            <Navbar.Collapse>
                <Nav className="ml-auto">
                    <Link to="/" className="nav-link">
                        Home
                    </Link>
                    <Link to="/courses" className="nav-link">
                        Courses
                    </Link>
                    <Link to="/courses/add" className="nav-link"> 
                        Add Course
                    </Link>
                    <Link to="/courses/update" className="nav-link"> 
                        Update Course
                    </Link>
                    {user.id !== null ? 
                    <Link to="/logout" className="nav-link">
                        Logout
                    </Link> 
                    :
                    <>
                    <Link to="/register" className="nav-link">
                        Register
                    </Link>
                    <Link to="/login" className="nav-link">
                        Login
                    </Link>
                    </>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
};

