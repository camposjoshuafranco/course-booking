import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export const CourseCard = ({courseProp}) => {
    return (
                <Card className="my-3 px-5">
                    <Card.Body>
                        <Card.Title>
                            {courseProp.name}   
                        </Card.Title>
                        <Card.Text>
                            {courseProp.description}
                        </Card.Text>
                        <Card.Text>
                            Price: {courseProp.price}
                        </Card.Text>
                        <Link to={`/courses/view/${courseProp._id}`} className=" btn btn-success btn-main">View Course</Link>
                    </Card.Body>
                </Card>
    )
}